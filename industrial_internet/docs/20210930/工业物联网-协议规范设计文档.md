# 消息通信

更新时间：2021-09-25

## 通信方式

设备与物联网平台、网关与物联网平台的上下行通信数据传输协议，采用mqtt协议，每个设备间通信的通道和权限都独立，设备A只能监听和订阅设备A相关的消息topic，做到topic级别权限隔离。

设备与物联网平台消息流转，物联网平台与第三方应用数据流转，主要通过 Pulsar 消息中间件，以满足合作伙伴对消息实时性和消息持久化的需求。

## Pulsar

**简介**

Pulsar 是一个支持多租户、高性能的服务器到服务器之间消息通讯的解决方案。Pulsar 最初由雅虎开发，现在由 Apache 软件基金会管理。物联网平台基于开源的 Pulsar 系统进行了定制改进。 Pulsar 作为消息代理采用了Pub/Sub（发布订阅）的设计模式。该设计模式中，生产者将消息发布到主题，然后消费者可以订阅这些主题，处理传入消息，并在处理完成时发送确认。 当订阅被创建时（即使消息处理设备已断开连接）所有的消息都将被 Pulsar 保留。只有在消息处理设备确认消息被成功处理后，保留下来的消息才会被丢弃。 此外，一个主题可以由多个消费者订阅，并且当消费者成功处理消息时，它需要向代理发送确认，以便代理可以丢弃该消息。涂鸦智能的 Pulsar 消息分发器（Broker）为每个主题分配了多个分区，Pulsar 消息分发器将根据分区和消费者分发消息。

**安全**

-   认证安全

    物联网平台 Pulsar 消息系统针对身份认证进行了深度定制以满足高安全性要求，物联网平台采用动态令牌机制增强安全，开发者可忽略实现细节，基于物联网平台提供的设备接入 SDK 完成认证。

-   数据安全

    传输安全： Pulsar 消息推送系统基于 SSL 传输数据。

# Topic说明

更新时间：2021-09-25

Topic是消息发布（Pub）者和订阅（Sub）者之间的传输中介。设备可通过Topic实现消息的发送和接收，从而实现服务端与设备端的通信。为方便海量设备基于Topic进行通信，简化授权操作，物联网平台定义了产品Topic类和设备Topic。本文介绍产品和设备Topic的定义、使用和分类。

## Topic定义

产品Topic类：产品维度的Topic，是同一产品下不同设备的Topic集合。一个Topic类对一个**ProductKey**下所有设备通用。

以下是Topic类的使用说明：

定义Topic的功能。

Topic类格式以正斜线（/）进行分层，区分每个类目。

-   消息需要持久存储的topic示例：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.xxx`。
-   消息不需要持久存储的topic示例：`non-persistent://${tenantid}/${user}/${productKey}.${deviceName}.xxx`。

其中，消息是否需要持久存储，物联网平台会根据topic功能，和数据重要程度，会自动区分，不需要用户自行配置。`${tenantid}/${user}/${productKey}`会在产品创建完后自动替换生成。

${tenantid} 表示设备厂商或开发者用户的租户id，可以试注册账号的所属公司、组织的标识

${user} 表示创建产品、设备的用户唯一标识，目前取值**default**，暂时不对用户分配

${productKey} 表示产品的标识符**ProductKey**。在指定产品的Topic类中，需替换为实际的**ProductKey**值

${deviceName} 表示设备名称**DeviceName**。在产品Topic类中，${deviceName}是该产品下所有设备的名称变量

xxx 表示具体的功能定义topic，可以有多层，以点 “.” 分隔，比如sys.event.post，sys.command.get等



2、定义Topic的操作权限。

-   **发布**：产品下设备可以往该Topic发布消息。
-   **订阅**：产品下设备可以订阅该Topic，从而获取消息。
-   **发布和订阅**：同时具备**发布**和**订阅**的操作权限。

## 设备Topic说明

在产品Topic类基础上，使用`${tenantid}/${user}/${productKey}.${deviceName}.xxx`通配一个唯一的设备，与前缀、后缀类目组成的完整Topic，就是具体的设备Topic。

设备Topic与产品Topic类格式一致，区别在于Topic类中的变量${deviceName}，在设备Topic中是具体的设备名称（**DeviceName**）。

例如用户租户为`talkweb`的产品`a19mzPZ***`下设备`device1`和`device2`的具体Topic如下：

-   `persistent://talkweb/defalut/a19mzPZ***.device1.xxx`
-   `persistent://talkweb/defalut/a19mzPZ***.device2.xxx`

产品Topic类定义的功能和操作权限，会映射到具体的设备Topic。以下是设备Topic的使用说明：

-    具体的设备Topic可用于消息通信。在上下行测试时，请注意topic权限
-   指定的设备Topic只能被指定设备用于消息通信。如topic `persistent://talkweb/defalut/a19mzPZ***.device1.xxx归属于设备device1，只能被设备device1用于发布或订阅消息，而不能被设备device2用于发布或订阅消息。
-   设备的通信收发功能可以被禁用，如果发现异常行为可以在平台操作禁用设备

## 生成设备Topic

设备Topic自动生成流程如下

![image-20210911165634591](image/image-20210911165634591.png)

1.  创建产品后，物联网平台为该产品预定义了Topic类，包含**基础通信Topic**、**物模型通信Topic**和**自定义Topic**。
2.  添加设备后，产品的所有Topic类会自动映射到设备上，生成具体的设备Topic。产品Topic类定义的操作权限，会映射到具体的设备Topic。您无需单独为每个设备创建Topic。
3.  开发设备，将设备联网上线，接入物联网平台。



## Topic权限说明

-   指定的设备Topic只能被指定设备用于消息通信。如topic `persistent://talkweb/defalut/a19mzPZ***.device1.xxx`归属于设备device1，只能被设备device1用于发布或订阅消息，而不能被设备device2用于发布或订阅消息。
-   子设备的消息通信是借用网关设备的物理通道进行通信，所以网关有发布和订阅所有子设备的消息通信权限。前提是所有的网关子设备也必须经过平台的认证、同时必须通过网关-子设备的拓扑关系建立。

## 通过设备Topic通信

<img src="image/image-20210930094615113.png" alt="image-20210930094615113" style="zoom:50%;" />

1.  设备发送消息到物联网平台。设备通过具有发布权限Topic，向物联网平台发送消息。
2.  物联网平台流转数据到服务器。物联网平台流转设备消息到应用服务器、其他Topic或其他消息中间件处理。
3.  服务器远程控制设备。应用服务器调用物联网平台的云端API，向指定设备发送消息。
4.  物联网平台下发消息到设备。在物联网平台控制台，可以直接通过已订阅的Topic，向指定设备下发指令，或发送消息。

## Topic分类和通信说明

物联网平台预定义的产品Topic类包含基础通信Topic、物模型通信Topic和自定义Topic三类Topic，详细说明见下表。

**基础通信Topic**

| Topic类  | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| OTA升级  | OTA升级消息的Topic，包括设备上报OTA模块版本、物联网平台推送升级包信息、设备上报升级进度和设备请求获取最新升级包信息。 |
| 设备标签 | 上报设备标签的Topic，上报设备的部分信息，如厂商、设备型号等。 |
| 时钟同步 | NTP服务同步请求和响应的Topic，解决嵌入式设备资源受限，系统不包含NTP服务，端上没有精确时间戳的问题。 |
| 设备影子 | 设备影子数据通过Topic进行流转，包括设备影子发布和设备接收影子变更。 |
| 配置更新 | 设备主动请求配置信息和物联网平台推送配置信息的Topic。开发人员可在不用重启设备或中断设备运行的情况下，在线远程更新设备的系统参数、网络参数等配置信息。 |

**物模型通信Topic**

| Topic类  | 说明                           |
| -------- | ------------------------------ |
| 属性上报 | 设备属性上报到平台             |
| 属性设置 | 平台对设备可写属性进行写入操作 |
| 事件上报 | 设备发生的事件信息上报到平台   |
| 命令调用 | 下发命令给设备，执行复杂的操作 |

**自定义Topic**

自定义Topic类及格式。系统默认提供了3个自定义Topic类。您可根据业务需求，自定义Topic类。

Topic类是一个Topic模版配置，编辑更新某个Topic类后，可能对产品下所有设备使用该类Topic通信产生影响。建议在设备研发阶段设计好，设备上线后不再变更Topic类。



# TLink协议

更新时间：2021-09-25

## TLink协议说明

TLink协议是针对物联网开发领域设计的一种数据交换规范，数据格式是JSON，用于设备端和物联网平台的双向通信，更便捷地实现和规范了设备端和物联网平台之间的业务数据交互。

物联网平台为设备端开发提供了SDK，这些SDK已封装了设备端与物联网平台的交互协议。您可以直接使用设备端Link SDK来进行开发。如果嵌入式环境复杂，已提供的设备端Link SDK不能满足您的需求，请参见本文，自行封装Tink协议数据，建立设备与物联网平台的通信。



## 设备身份注册

设备第一次上线之前您需要对设备进行身份注册，标识您的设备，可接受同一设备的多次注册。

已注册的设备，后续的每次设备断电后重新上线时都会先经过平台认证，认证通过后，设备才能与平台建连，进行后续的通信。

**一机一密的直连设备**会先在平台预注册，同时也会将设备三元组（ProductKey、DeviceName、DeviceSecret）烧录到设备上，可做设备的唯一认证信息，对于这种设备，不需要注册过程，第一次建连就是一个认证上线过程。

**一型一密的直连设备**会在平台预注册，但是烧录到设备上的信息只包含ProductKey、DeviceName，ProductSecret，不能做设备的认证信息，必须先要进行注册，从平台获取具体设备三元组信息（ProductKey、DeviceName、DeviceSecret），存储到本地，再做认证上线的逻辑。

**一型一密的网关子设备**需要先到平台进行预注册，子设备借助网关的物理通道进行MQTT动态注册，获取DeviceSecret。



### 直连设备一型一密设备的HTTP/HTTPS动态注册

-   URL模板：`http://host.com/twiot-device-server/device/register`
-   HTTP方法：POST

请求数据格式：

```
 POST /iot-auth/register/device  HTTP/1.1
 Host: host.com
 Content-Type: application/x-www-form-urlencoded
 Content-Length: 123
 productKey=a1234*****&deviceName=sn1234&timestamp=567345&sign=adfv123hdfdh&signMethod=hmacmd5
```

响应数据格式：

```
 {
   "code": {
     "code": 0,
     "msg": "操作成功"
   },
   "result": {
     "productKey": "a1234******",
     "deviceName": "deviceName1234",
     "deviceSecret": "adsfw******"
   }
 }
```

参数说明如下表。

| 参数          | 类型    | 说明                                                         |
| ------------- | ------- | ------------------------------------------------------------ |
| Method        | String  | POST                                                         |
| Host          | String  | 设备认证服务器地址Endpoint地址                               |
| Content-Type  | String  | 设备发送给物联网平台的上行数据的编码格式。                   |
| productKey    | String  | 产品唯一标识。                                               |
| productSecret | String  | 产品秘钥                                                     |
| deviceName    | String  | 设备名称。请注意，这里的设备名称，请使用物联网平台预注册设备时约定使用的设备唯一信息，如设备的MAC地址、IMEI或SN码等，例如，物联网平台使用SN码预注册设备，则设备第一次注册认证时，请将deviceName赋值SN码进行动态注册 |
| timestamp     | String  | 时间戳                                                       |
| sign          | String  | 签名。 加签方法： 将所有提交给服务器的参数（sign、signMethod除外）按照字母顺序排序，然后将参数和值依次拼接（**无拼接符号**）。 通过signMethod指定的加签算法，使用产品的**ProductSecret**，对加签内容进行加签。 加签计算示例如下： `hmac_md5(productSecret, deviceNamedeviceName1234productKeya1234******timestamp123)` |
| signMethod    | String  | 签名方法，目前支持hmacmd5、hmacsha1、hmacsha256。            |
| code          | Integer | 结果信息。0为成功，非0则认证失败                             |
| msg           | String  | 此次请求结果说明，成功或失败，具体的失败原因等信息           |
| deviceSecret  | String  | 设备密钥。                                                   |

### 网关子设备MQTT动态注册

请求topic定义：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.register`

topic中的productKey和deviceName为网关的信息

请求数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "sub.register",
   "spec":{
       "ack":0
   },
   "data": {
     "deviceName": "deviceName1234",
     "productKey": "a1234******"
   }
 }
```

参数列表

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String   | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long     | 消息发送时时间戳                                             |
| method     | String   | 请求方法。取值：`sys.sub.register`。                         |
| spec       | Object   | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer  | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | Object   | 子设备动态注册的参数。                                       |
| deviceName | String   | 子设备的名称。                                               |
| productKey | String   | 子设备的产品ProductKey。                                     |

响应topic定义： `persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.register_reply`

响应数据格式：

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {
     "productKey": "a1234******",
     "deviceName": "deviceName1234",
     "deviceSecret": "xxxxxx"
   }
 }
```

参数列表

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的数据。固定为空。                           |



## 设备建连认证

**设备或网关设备**与物联网平台通过MQTT协议连接通信，在进行通信前，设备必须与MQTT服务端进行认证，认证通过后，设备才能发布和订阅消息，本文主要对MQTT CONNECT协议规范进行说明。

在进行MQTT CONNECT协议设置时，需注意：

-   如果同一个设备证书（ProductKey、DeviceName和DeviceSecret）同时用于多个物理设备连接，可能会导致客户端频繁上下线。因为新设备连接认证时，原设备会被迫下线，而设备被下线后，又会自动尝试重新连接。
-   （可选）为保障通信安全，推荐使用TLS加密的连接方式。

**认证协议规范**

建议直接使用物联网平台提供的设备SDK接入物联网平台，如果需要自行开发接入，连接参数如下。

**1、参数：接入域名、IP。**根据具体的平台部署情况，选择合适的地址接入

**2、参数：可变报头（variable header）：Keep Alive。**

CONNECT指令中需包含Keep Alive（保活时间）。保活心跳时间取值范围为30秒~1200秒，建议取值300秒以上。若网络不稳定，请将心跳时间设置长一些。如果心跳时间不在保活时间内，物联网平台会拒绝连接。

**3、参数：MQTT的CONNECT报文。**

一机一密设备认证方式：使用设备证书（ProductKey、DeviceName和DeviceSecret）连接。

一型一密预注册设备，需要先从平台获取设备证书（ProductKey、DeviceName和DeviceSecret），完成获取设备证书后， 认证方式同一机一密设备认证。

所以，一机一密设备认证，一型一密预注册设备认证最终的MQTT连接认证方式相同。

参数如下：

```
 mqttClientid: clientId+"|clienttype=0,signmethod=hmacmd5,timestamp=132323232|"
 mqttUsername: deviceName+"&"+productKey
 mqttPassword: sign_hmac(deviceSecret,content)
```

-   mqttClientId：格式中| |内为扩展参数。
-   clientId：表示客户端ID，可自定义，长度不可超过64个字符。建议使用设备的MAC地址或SN码，方便您识别区分不同的客户端。
-   clienttype：标识客户端类型，0：直连设备/网关设备，1：appuser，app端用户认证（app也是作为一个通用设备接入mqtt组件中）。可不传，如果不传则默认为0。
-   signmethod：表示签名算法类型。支持hmacmd5，hmacsha1和hmacsha256，默认为hmacmd5。
-   timestamp：表示当前时间毫秒值。
-   mqttPassword：sign签名需把提交给服务器的参数按字典排序后，根据signmethod加签。签名计算示例下文有说明。
-   content的值为提交给服务器的参数（productKey、deviceName、clientId、timestamp），按照参数名称首字母字典排序， 然后将参数值依次拼接。

示例：

假设`clientId = 12345，deviceName = device1， productKey = pk， timestamp = 789，signmethod=hmacmd5，deviceSecret=secret123`，那么使用TCP方式提交给MQTT的参数如下：

```
 mqttclientId=12345|signmethod=hmacmd5,timestamp=789|
 mqttUsername=device&pk
 mqttPassword=hmacmd5("secret123","clientId12345deviceNamedeviceproductKeypktimestamp789"); 
```

加密后的Password的值示例为：`5d7845ac6ee7cfffafc5fe5f35cf666d`

## 管理网关与子设备拓扑关系

子设备身份注册后，需由网关向物联网平台上报网关与子设备的拓扑关系，然后进行子设备上线。

子设备上线过程中，物联网平台会校验子设备的身份和与网关的拓扑关系。所有校验通过，才会建立并绑定子设备逻辑通道至网关物理通道上。子设备与物联网平台的数据上下行通信与直连设备的通信协议一致，协议上不需要露出网关信息。

删除拓扑关系后，子设备不能再通过网关上线。系统将提示拓扑关系不存在，认证不通过等错误。

### 添加设备拓扑关系

网关类型的设备，可以通过该Topic上行请求添加它和子设备之间的拓扑关系，返回成功添加拓扑关系的子设备。

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.add`

数据流向：网关设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "topo.add",
   "spec":{
       "ack":0
   },
   "data": [
     {
       "deviceName": "deviceName1234",
       "productKey": "1234556554",
       "sign": "xxxxxx",
       "signmethod": "hmacmd5",
       "timestamp": "1524448722000"
     },
     {
       "deviceName": "deviceName2345",
       "productKey": "1234556554",
       "sign": "xxxxxx",
       "signmethod": "hmacmd5",
       "timestamp": "1524448722000"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String   | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long     | 消息发送时时间戳                                             |
| method     | String   | 请求方法。取值：topo.add。                                   |
| spec       | Object   | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer  | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | List     | 请求入参                                                     |
| deviceName | String   | 子设备名称                                                   |
| productKey | String   | 子设备所属产品productKey                                     |
| sign       | String   | 签名。 加签算法： 将所有提交给服务器的参数（**sign**，**signMethod**除外）按照字母顺序排序，然后将参数和值依次拼接（无拼接符号）。 对加签内容，需通过signMethod指定的加签算法，使用设备的DeviceSecret值，进行签名计算。 `sign= hmac_md5(deviceSecret, clientId123deviceNametestproductKey123timestamp1524448722000)` |
| signmethod | String   | 签名方法，支持hmacmd5，hmacsha1和hmacsha256，默认为hmacmd5   |
| timestamp  | String   | 时间戳，毫秒。                                               |

响应topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.add_reply`

数据流向：平台发布消息，网关设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": [
     {
       "deviceName":"deviceName1234",
       "productKey":"1234556554"
     },
     {
       "deviceName":"deviceName2345",
       "productKey":"1234556554"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code       | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data       | Object   | 请求成功时，返回成功的设备信息。                             |
| devceName  | String   | 子设备的设备名称。                                           |
| productKey | String   | 子设备所属产品的ProductKey。                                 |

### 删除设备的拓扑关系

网关类型的设备，可以通过该Topic上行请求删除它和子设备之间的拓扑关系，返回成功删除拓扑关系的子设备。

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.delete`

数据流向：网关设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "topo.delete",
   "spec":{
       "ack":0
   },
   "data": [
     {
       "deviceName": "deviceName1234",
       "productKey": "1234556554",
     },
     {
       "deviceName": "deviceName2345",
       "productKey": "1234556554",
     }
   ]
 }
```

参数说明

| 参数       | 数据类型  | 说明                                                         |
| ---------- | --------- | ------------------------------------------------------------ |
| reqid      | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long      | 消息发送时时间戳                                             |
| method     | String    | 请求方法。取值：topo.delete。                                |
| spec       | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | List      | 请求入参                                                     |
| deviceName | String    | 子设备名称                                                   |
| productKey | String    | 子设备所属产品productKey                                     |

响应topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.delete_reply`

数据流向：平台发布消息，网关设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": [
     {
       "deviceName":"deviceName1234",
       "productKey":"1234556554"
     },
     {
       "deviceName":"deviceName2345",
       "productKey":"1234556554"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code       | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data       | Object   | 请求成功时，返回成功的设备信息。                             |
| deviceName | String   | 子设备名称                                                   |
| productKey | String   | 子设备所属产品productKey                                     |

### 通知网关拓扑关系变化

将拓扑关系变化通知网关。

| 操作             | 行为                                                 | 方式         |
| ---------------- | ---------------------------------------------------- | ------------ |
| 网关下添加子设备 | 添加网关与子设备的拓扑关系。                         | 平台通知网关 |
| 删除子设备       | 删除子设备与对应网关的拓扑关系。                     | 平台通知网关 |
| 禁用子设备       | 禁用子设备，并禁用当前子设备与对应网关的拓扑关系。   | 平台通知网关 |
| 启用子设备       | 解除子设备禁用，恢复当前子设备和对应网关的拓扑关系。 | 平台通知网关 |

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.change`

数据流向：平台发布消息，网关设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "topo.change",
   "spec":{
       "ack":0
   },
   "data": {
     "action":"add", //add-添加，delete-删除，disable-禁用， rec-disable-恢复禁用 
     "subList":[
       {
         "productKey":"a1hRrzD****",
         "deviceName":"abcd"
       },
       {
         "productKey":"b1hRrzD****",
         "deviceName":"cdef"
       }
     ]
   }
 }
```

参数说明

| 参数       | 数据类型  | 说明                                                         |
| ---------- | --------- | ------------------------------------------------------------ |
| reqid      | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long      | 消息发送时时间戳                                             |
| method     | String    | 请求方法。取值：`topo.change`。                              |
| spec       | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | Object    | 请求参数，包含执行动作和子设备列表                           |
| action     | String    | 上报的属性值，add-添加，delete-删除，disable-禁用， rec-disable-恢复禁用 |
| subList    | List      | 子设备列表                                                   |
| deviceName | String    | 子设备名称                                                   |
| productKey | String    | 子设备所属产品productKey                                     |

响应topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.topo.change_reply`

数据流向：网关设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {}
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回空。                                         |



## 子设备上下线

直连设备在建连认证通过后，即认为设备已上线。子设备，必须借助网关的联网能力，上报上线信息才能确定上线。子设备可以逐个上下线，也可以批量上下线。子设备上线之前，需在物联网平台为子设备注册身份，建立子设备与网关的拓扑关系。子设备上线时，物联网平台会根据拓扑关系进行子设备身份校验，以确定子设备是否具备使用网关通道的能力。

说明

-   一个网关下，同时在线的子设备数量不能超过1,500。在线子设备数量达到1,500个后，新的子设备上线请求将被拒绝。
-   发送子设备批量上下线请求时，单个批次上下线的子设备数量不超过5个。
-   设备批量上下线请求结果为全部成功或全部失败，失败时的**data**响应参数中会包含具体的设备信息。

### 子设备上线

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.login`

说明：因为子设备通过网关通道与物联网平台通信，以上Topic为网关设备的Topic。Topic中变量${productKey}和${deviceName}需替换为网关设备的对应信息。

数据流向：网关设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "sub.login",
   "spec":{
       "ack":0
   },
   "data": [
     {
       "productKey": "al12345****",
       "deviceName": "device1234",
       "timestamp": "1581417203000",
       "signMethod": "hmacmd5",
       "sign": "9B9C732412A4F84B981E1AB97CAB****"
     },
     {
       "productKey": "al12345****",
       "deviceName": "device1234",
       "timestamp": "1581417203000",
       "signMethod": "hmacmd5",
       "sign": "9B9C732412A4F84B981E1AB97CAB****"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String   | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long     | 消息发送时时间戳                                             |
| method     | String   | 请求方法。取值：`sub.login`。                                |
| spec       | Object   | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer  | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | List     | 请求上线的数据体，可多个设备同时上报                         |
| deviceName | String   | 子设备名称                                                   |
| productKey | String   | 子设备所属产品productKey                                     |
| sign       | String   | 签名。 加签算法： 将所有提交给服务器的参数（**sign**，**signMethod**除外）按照字母顺序排序，然后将参数和值依次拼接（无拼接符号）。 对加签内容，需通过signMethod指定的加签算法，使用设备的DeviceSecret值，进行签名计算。 `sign= hmac_md5(deviceSecret, clientId123deviceNametestproductKey123timestamp1524448722000)` |
| signMethod | String   | 签名方法，支持hmacmd5，hmacsha1和hmacsha256，默认为hmacmd5   |
| timestamp  | String   | 时间戳，毫秒                                                 |

响应topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.login_reply`

数据流向：平台发布消息，网关设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": [
     {
       "productKey": "al12345****",
       "deviceName": "device1234"
     },{
       "deviceName": "device4321",
       "productKey": "al12345****"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code       | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data       | List     | 请求成功时，返回的数据，固定为空。                           |
| productKey | String   | 子设备所属产品的ProductKey。                                 |
| deviceName | String   | 子设备的设备名称。                                           |

### 子设备下线

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.logout`

说明：因为子设备通过网关通道与物联网平台通信，以上Topic为网关设备的Topic。Topic中变量${productKey}和${deviceName}需替换为网关设备的对应信息。

数据流向：网关设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "sub.logout",
   "spec":{
       "ack":0
   },
   "data": [
     {
       "productKey": "al12345****",
       "deviceName": "device1234"
     },
     {
       "productKey": "al12345****",
       "deviceName": "device1234"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v          | String   | 协议版本号，目前协议版本号唯一取值为1                        |
| t          | Long     | 消息发送时时间戳                                             |
| method     | String   | 请求方法。取值：`sub.logout`。                               |
| spec       | Object   | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack        | Integer  | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data       | List     | 请求上线的数据体，可多个设备同时上报                         |
| deviceName | String   | 子设备名称                                                   |
| productKey | String   | 子设备所属产品productKey                                     |

响应topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.sub.logout_reply`

数据流向：平台发布消息，网关设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": [
     {
       "productKey": "al12345****",
       "deviceName": "device1234"
     },{
       "deviceName": "device4321",
       "productKey": "al12345****"
     }
   ]
 }
```

参数说明

| 参数       | 数据类型 | 说明                                                         |
| ---------- | -------- | ------------------------------------------------------------ |
| reqid      | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code       | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data       | List     | 请求成功时，返回的数据，固定为空                             |
| productKey | String   | 子设备所属产品的ProductKey                                   |
| deviceName | String   | 子设备的设备名称                                             |

## 设备属性

### 设备上报属性

上报属性请求topic定义：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.propertyReport.post`

数据流向：设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "event.propertyReport.post",
   "spec":{
       "ack":0
   },
   "data": {
     "Power": {
       "value": "on"
     },
     "WF": {
       "value": 23.6,
       "t": 1524448722000
     }
   }
 }
```

参数说明

| 参数   | 数据类型  | 说明                                                         |
| ------ | --------- | ------------------------------------------------------------ |
| reqid  | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v      | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t      | Long      | 消息发送时时间戳                                             |
| method | String    | 请求方法。取值：`event.propertyReport.post`。                |
| spec   | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack    | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data   | Object    | 请求参数。如以上示例中，设备上报了的两个属性Power（电源）和WF（工作电流）的信息。具体属性信息，包含属性上报时间（t）和上报的属性值（value），上报时间可根据具体的场景不传，如果不传，将会统一取协议体里的t字段。 如果是自定义模块属性，属性标识符格式为`模块标识符:属性标识符`（中间为英文冒号）。例如： `"test:Power": {      "value": "on",      "time": 1524448722000    }` |
| value  | String    | 上报的属性值                                                 |

上报属性响应topic说明： `persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.propertyReport.post_reply`

数据流向：平台发布消息，设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {}
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的数据。固定为空。                           |

### 设置设备属性

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.setProperty.exec`

数据流向：平台发布消息，设备监听消息

请求数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "command.setProperty",
   "spec":{
     "ack":0
   },
   "data": {
     "temperature": "30.5"
   }
 }
```

参数说明

| 参数   | 数据类型  | 说明                                                         |
| ------ | --------- | ------------------------------------------------------------ |
| reqid  | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v      | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t      | Long      | 消息发送时时间戳                                             |
| method | String    | 请求方法。取值：`command.property.set`。                     |
| spec   | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack    | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data   | Object    | 属性设置参数。如以上示例中，设置属性：`{ "temperature": "30.5" }`。 如果是自定义模块属性，属性标识符格式为`模块标识符:属性标识符`（中间为英文冒号），例如`{ "test:temperature": "30.5" }`。 |

设置属性响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.setProperty.exec_reply`

数据流向：设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {}
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的数据。固定为空。                           |



### 获取设备属性

平台可主动发送获取设备属性的命令到设备，设备反馈平台需要获取的设备信息上报平台

请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.getProperty.exec`

数据流向：平台发布消息，设备监听消息

请求数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "command.getProperty",
   "spec":{
     "ack":1
   },
   "data": [
     "Status",
     "Data",
     "DeviceInfo",
     "Volume"
   ]
 }
```

参数说明

| 参数   | 数据类型  | 说明                                                         |
| ------ | --------- | ------------------------------------------------------------ |
| reqid  | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v      | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t      | Long      | 消息发送时时间戳                                             |
| method | String    | 请求方法。取值：`command.getProperty`。                      |
| spec   | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack    | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data   | Object    | 需要获取数据的属性列表。如以上示例代码示例中，需要获取四个属性，`"Status","Data","DeviceInfo","Volume"`。 如果是自定义模块属性，属性标识符格式为`模块标识符:属性标识符`（中间为英文冒号），例如`[ "test:temperature"]`。 |

设置属性响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.getProperty.exec_reply`

数据流向：设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {
     "Status":"1",
     "Data":"设备告警",
     "DeviceInfo":["鸿蒙","3861"],
     "Volume":"0"
   }
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的设备数据。                                 |



## 设备事件

默认模块请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.${tsl.event.pid}.post`

自定义模块请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.${tsl.blockId}:${tsl.event.pid}.post`

这里的${tsl.event.pid}是指物模型中定义的事件唯一标识，如果是自定义模块中的事件，topic中 ${tsl.blockId}:${tsl.event.pid}，冒号前面是指产品自定义模块唯一编码，冒号后面指此自定义模块中物模型事件唯一标识

数据流向：设备发布消息，平台监听消息

请求数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "event.${tsl.event.pid}.post",
   "spec":{
     "ack":0
   },
   "data": {
     "temperature": "30.5"
   }
 }
```

参数说明

| 参数   | 数据类型 | 说明                                                         |
| ------ | -------- | ------------------------------------------------------------ |
| reqid  | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v      | String   | 协议版本号，目前协议版本号唯一取值为1                        |
| t      | Long     | 消息发送时时间戳                                             |
| method | String   | 请求方法。取值：`event.${tsl.event.tid}.post` 或`event.${tsl.blockId}:${tsl.event.tid}.post` ${tsl.event.tid}是指物模型中定义的事件唯一标识，${tsl.blockId}:${tsl.event.tid}是指自定义模块中事件唯一标识。 |
| spec   | Object   | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack    | Integer  | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data   | Object   | 事件输出参数。如以上示例中，设置属性：`{ "temperature": "30.5" }`。 |

默认模块响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.${tsl.event.pid}.post_reply`

自定义模块响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.event.${tsl.blockId}:${tsl.event.pid}.post_reply`

数据流向：平台发布消息，设备监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {}
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的数据。固定为空。                           |

格式示例 

假设产品中定义了一个alarm事件，它的TSL描述如下。

```
 {
   "profile": {
     "version": 1,
     "productKey": "${productKey}",
     "deviceName": "airCondition"
   },
   "events": [
     {
       "pid": "alarm",
       "name": "alarm",
       "desc": "风扇警报",
       "type": "alert",
       "required": true,
       "outputData": [
         {
           "pid": "errorCode",
           "name": "错误码",
           "dataType": {
             "type": "text",
             "specs": {
               "length": "255"
             }
           }
         }
       ],
       "method": "event.alarm.post"
     }
   ]
 }
```

则它上报事件时，TLink 数据格式如下：

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "event.alarm.post",
   "spec":{
     "ack":0
   },
   "data": {
     "errorCode": "error!"
   }
 }
```



## 设备命令

默认模块请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.${tsl.command.pid}.exec`

自定义模块请求topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.${tsl.blockId}:${tsl.command.pid}.exec`

这里的${tsl.command.pid}是指物模型中定义的命令唯一标识，如果是自定义模块中的事件，topic中 ${tsl.blockId}:${tsl.command.pid}，冒号前面是指产品自定义模块唯一编码，冒号后面指此自定义模块中物模型命令唯一标识

数据流向：平台发布消息，设备监听消息

请求数据格式：

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "command.${tsl.command.pid}.exec",
   "spec":{
     "ack":0
   },
   "data": {
     "temperature": "30.5"
   }
 }
```

参数说明：

| 参数   | 数据类型  | 说明                                                         |
| ------ | --------- | ------------------------------------------------------------ |
| reqid  | String 32 | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| v      | String    | 协议版本号，目前协议版本号唯一取值为1                        |
| t      | Long      | 消息发送时时间戳                                             |
| method | String    | 请求方法。取值：`command.${tsl.commandevent.pid}.exec` 或`command.${tsl.blockId}:${tsl.command.pid}.exec ${tsl.command.pid}是指物模型中定义的事件唯一标识，${tsl.blockId}:${tsl.command.pid}是指自定义模块中事件唯一标识。 |
| spec   | Object    | 扩展功能的参数，其下包含各功能字段。平台可扩展，或可自行扩展，自行扩展的参数需在自定义解析模块自行解析 |
| ack    | Integer   | 扩展功能字段，表示是否返回响应数据。 0：不返回响应数据 1：返回响应数据 |
| data   | Object    | 事件输出参数。如以上示例中，设置属性：`{ "temperature": "30.5" }`。 |

默认模块响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.${tsl.command.pid}.exec_reply`

自定义模块响应topic说明：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.sys.command.${tsl.blockId}:${tsl.command.pid}.exec_reply`

数据流向：设备发布消息，平台监听消息

数据格式

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {}
 }
```

参数说明

| 参数  | 数据类型 | 说明                                                         |
| ----- | -------- | ------------------------------------------------------------ |
| reqid | String   | 消息ID号。uuid，去掉短横线，32位，全局唯一，用于ack或系统消息追踪 |
| code  | Integer  | 结果状态码。成功为200，失败时，具体的错误码，请参考错误码说明章节 |
| data  | Object   | 请求成功时，返回的数据。固定为空。                           |

格式示例

假设产品中定义了一个SetWeight命令，它的TSL描述如下。

```
 {
   "profile": {
     "version": 1,
     "productKey": "testProduct01"
   },
   "commands": [
     {
       "pid": "SetWeight",
       "name": "设置重量",
       "required": false,
       "callType": "async",
       "outputData": [
         {
           "pid": "OldWeight",
           "dataType": {
             "specs": {
               "unit": "kg",
               "min": "0",
               "max": "200",
               "step": "1"
             },
             "type": "double"
           },
           "name": "OldWeight"
         },
         {
           "pid": "CollectTime",
           "dataType": {
             "specs": {
               "length": "2048"
             },
             "type": "text"
           },
           "name": "CollectTime"
         }
       ],
       "inputData": [
         {
           "pid": "NewWeight",
           "dataType": {
             "specs": {
               "unit": "kg",
               "min": "0",
               "max": "200",
               "step": "1"
             },
             "type": "double"
           },
           "name": "NewWeight"
         }
       ],
       "method": "command.SetWeight.exec"
     }
   ]
 }
```

当命令平台触发执行时，平台请求数据格式为

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "command.SetWeight.exec",
   "spec":{
     "ack":0
   },
   "data": {
     "NewWeight": 100.8
   }
 }
```

设备响应的数据格式如下

```
 {
   "reqid": "105917531",
   "code": 200,
   "data": {
     "CollectTime": "1536228947682",
     "OldWeight": 100.101
   }
 }
```



## NTP服务

物联网平台提供NTP服务，为资源受限的嵌入式设备，解决无法实时地获取服务端时间的问题。

### 原理介绍

物联网平台的NTP服务，借鉴NTP协议原理，将物联网平台作为NTP服务器。高精准度的时间校正流程如下：

-   设备端通过指定Topic向物联网平台发送消息，会携带发送时间**deviceSendTime**。
-   物联网平台接收设备端消息后，回复消息中，会增加接收消息的时间**serverRecvTime**和回复消息的时间**serverSendTime**。
-   设备端接收到物联网平台回复，会根据本地时间，给出接收回复的时间**deviceRecvTime**。
-   根据以上出现的4个时间，计算设备端与物联网平台的时间差，得出设备端获取到的，服务端当前的精确时间**Time**。

**注意：** 仅当设备端成功接入物联网平台后，才能通过NTP服务进行时间校准。若嵌入式设备未接入物联网平台，设备上电后，无法通过NTP服务进行时间校准，则TSL建连过程中，证书时间校验会失败。

### 通信Topic

请求Topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp`

响应Topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp_reply`

### 设备端接入说明

NTP服务使用流程，及其Topic说明如下：

1、设备端订阅Topic：`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp_reply`

2、设备端向Topic ： `persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp` 发送一条消息，携带此设备的时间戳，单位为毫秒，数据协议如下

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "v": "1",
   "t": 1630054074378,
   "method": "ext.ntp",
   "spec":{
     "ack":1
   },
   "data": {
     "deviceSendTime": "1632794700082"
   }
 }
```

3、设备端通过`persistent://${tenantid}/${user}/${productKey}.${deviceName}.ext.ntp_reply`收到物联网平台回复的消息，数据协议如下：

```
 {
   "reqid": "0020fdf71f0d491da12ba3cd38b205be",
   "code": 200,
   "data": {
     "deviceSendTime":"1632794700082",
     "serverRecvTime":"1632794940081",
     "serverSendTime":"1632794951085",
   }
 }
```

4、设备端计算出服务端当前精确的Unix时间。

设备端收到服务端的时间记为${deviceRecvTime}，则设备上的精确时间为：`(${serverRecvTime}+${serverSendTime}+${deviceRecvTime}-${deviceSendTime})/2`。



### 使用示例

例如，设备上时间是1632794700000，服务端时间是1632794700100，此时设备和服务端时间相差100ms， 链路延时是10ms，服务端从接收到发送间隔为5毫秒。

| 操作       | 设备端时间（ms）                | 服务端时间（ms）                |
| ---------- | ------------------------------- | ------------------------------- |
| 设备端发送 | 1632794700000（deviceSendTime） | 1632794700100                   |
| 服务端接收 | 1632794700010                   | 1632794700110（serverRecvTime） |
| 服务端发送 | 1632794700015                   | 1632794700115（serverSendTime） |
| 设备端接收 | 1632794700025（deviceRecvTime） | 1632794700125                   |

则设备端计算出的当前准确时间为`(1632794700110+1632794700115+1632794700025-1632794700000)ms÷2=1632794700125ms`。

如果直接采用物联网平台返回的时间戳，只能得到时间1632794700115ms，与服务端上的时间会有10毫秒的链路延时误差。




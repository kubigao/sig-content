# 合规SIG例会 2022-08-12 14:15-15:30(UTC+08:00)Beijing

## 议题Agenda
- 合规SIG成立开工 陈雅旬
- 合规SIG整体规划第一次讨论 陈雅旬; 高亮
- 合规SIG初期基础事务讨论 高亮

## 与会人Attendees
- 陈雅旬、高琨、郑志鹏、王意明、王亮、丛林、Swann(Oniro)、高亮 等

## 纪要Notes
- 会议纪要见

https://zulip.openharmony.cn/#narrow/stream/62-compliance_sig/topic/Meeting20220812

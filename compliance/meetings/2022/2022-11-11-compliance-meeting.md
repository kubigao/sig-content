# 合规SIG例会 2022-11-11 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
- OpenHarmony许可证指导&特殊许可证评审  余甜
- OpenHarmony社区合规类issue管理流程评审    高亮


## 与会人Attendees
-  陈雅旬、高琨、郑志鹏、丛林、余甜、方晓、周乃鑫、高亮

## 纪要Notes
- 议题1、OpenHarmony许可证指导&特殊许可证评审

会议内容

1． 同意许可证评审流程，内容见https://gitee.com/openharmony/docs/pulls/12603/files

2． 补充完成LicenseMgr 需要的格式license数据

- 议题2、OpenHarmony社区合规类issue管理流程评审

会议结论：

1． 同意合规类issue管理流程，内容见https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E5%BC%80%E6%BA%90%E5%90%88%E8%A7%84%E7%B1%BB%E9%97%AE%E9%A2%98%E7%AE%A1%E7%90%86.md

2． 相关内容流程合入docs仓





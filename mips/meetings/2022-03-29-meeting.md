# Mar.29, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|同步X2000当前适配进展|洪涛 漆鹏振 袁祥仁|
|10:15-10:25|了解X2000推进计划表|洪涛 漆鹏振 袁祥仁 黄首西|
|10:25-10:30|遗留问题处理|王栋 黄首西|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- @漆鹏振
- [@wicom](https://gitee.com/wicom)
- [@xiongtao](https://gitee.com/xiongtao)
- @黄慧进
- @陈国栋
- @陈牡丹
- [@libing23](https://gitee.com/libing23)
- @王栋
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)

## Notes

#### 议题一、同步X2000当前适配进展

**结论**

- 修改内核vendor和device目录结构，适配仓库Master分支正在进行中。
- DFX子系统正在适配，其中hilog已完成，hiview,hievent完成部分。
- 独立模块合并主干，其中ai_engine完成合并，graphic部分正在进行中。


#### 议题二、了解X2000推进计划表

**结论**

- 君正已列出推进计划表，目前处于第一阶段。


#### 议题三、遗留问题

  **结论**

- MIPS编译子系统适配当前使用clang 11版本，还存在一些编译问题需要解决。下一步替换成社区clang10的版本。（本次会议，编译子系统人员未参会，会后联系沟通进展）

- 确认根据HDI接口实现linux兼容层的方式是否满足xts认证需求。（XTS子系统答复可以满足，但建议完整适配HDF驱动）

## Action items

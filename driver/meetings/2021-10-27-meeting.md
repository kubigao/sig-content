#  October 27, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                                       |
  | :---------: | ------------------------------------ | ------------------------------------------------------------ |
  | 16:00-16:15 | 议题一、Media的codec驱动能力开发进展 | crescenthe，vb6174，starfish，magekkkk，zhangguorong，zianed |
  | 16:10-16:40 | 议题二、传感器ppg开发经验分享        | 郭超胜，Muffin ，武和波 ，徐鹏飞，muffin，zianed，Kevin-Lau，NickYang |

## Attendees

- @[crescenthe](https://gitee.com/crescenthe)
- @[zianed](https://gitee.com/zianed)
- @郭超胜
- @Muffin 
- @武和波 
- @徐鹏飞 
- muffin（https://gitee.com/gitmuffin）
- [NickYang](https://gitee.com/haizhouyang)
- @[Kevin-Lau](https://gitee.com/Kevin-Lau)
- @[vb6174](https://gitee.com/vb6174)
- @[magekkkk](https://gitee.com/magekkkk)
- @xzmu(kelei@iscas.ac.cn)
- @ starfish(https://gitee.com/starfish002)
- @[zhangguorong](zhangguorong050@chinasoftinc.com)

## Notes

- **议题一、Media的codec驱动能力开发进展**

   结论：1、基于HDF驱动模型，已启动codec编码，完成框架编解码输入输出队列管理，配置等模块开发，需要补充功能测试用例，并验证通过后，按照openharmony要求，以企业贡献方式提交到master分支。后续模块按照开发计划提交代码到master分支。 
   
   2、修改的codec HDI接口需要在11月5号前完成修改和验证，有张少远提交上master分支。 。
   
- **议题二、Media的codec HDI接口修改评审**
   结论：1、有芯海科技穆凡老师和吴和波老师给大家分享ppg驱动在标准系统和小型系统上开发经验，并分享在开发过程遇到的问题和解题思路，并对传感器中的gpio和spi配置提出改进建议和改进措施。 

   2、当前ppg驱动开发已完成，在小型系统用demo程序完成验证；标准系统上，ppg器件上报的ADC数据正常。按照openharmony代码上库要求优化后，启动代码上库master分支流程。 
   
   遗留问题： 
   
   1、 贡献代码怎么上库？是否有流程要求？
   
   Openharmony贡献代码上库流程：https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md
   
   2、 代码贡献上哪个分支？ 
   
   贡献的代码完成功能自验证后，提交master分支PR，发起评审和review。 
   
   
   
   会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/UZUFCOPTOGRSWAZGSZSBF4GLSP2YQ3XX/
   
   会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 
   
   会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items

  


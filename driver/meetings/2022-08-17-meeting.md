#  August 17, 2022 at 16:00am-17:30am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力开发进展 | 张国荣，翟海鹏，刘飞虎，孙赫赫 |
  | 16:10-16:25 | 议题二、Sensor模块新增light HDI接口评审 | 赵文华，刘飞虎，袁博，翟海鹏，刘洪刚，周延旭, 黄一宏 |
  | 16:25-16:40 | 议题三、wlan模块HDI新增数据类型评审 | 赵文华，刘飞虎，袁博，刘飞虎，张云虎，刘洪刚，黄一宏 |

## Notes

- **议题一、Media的codec驱动能力开发进展----张国荣，翟海鹏，刘飞虎，孙赫赫**

  议题进展：Codec HDI1.0实现根据新的头文件适配ok，已提交待合入，支撑HDI2.0到1.0对接，BufferHandle支持开发已完成，多任务支持开发进度40%；Codec HDI2.0 idl化改造开发完成，支撑媒体服务联调。

- **议题二、Sensor模块新增light HDI接口评审----赵文华，刘飞虎，袁博，翟海鹏，刘洪刚，周延旭, 黄一宏**

  议题结论：对Sensor模块新增HDI接口和数据类型进行评审，新增1个接口和一个结构体，接口定义符合合规、兼容性和扩展性要求。

  HDI接口列表：

  | 接口                                                         | 评审结论 |
  | ------------------------------------------------------------ | -------- |
  | int32_t TurnOnMultiLights([in] int lightId, [in]const struct LightColor colors); | 通过     |

  遗留问题： 1.    lightId定义是否可以按照分组的方式定义，与lightId区分开，与硬件服务对齐。----周延旭

- **议题三、wlan模块HDI新增数据类型评审----赵文华，刘飞虎，袁博，刘飞虎，张云虎，刘洪刚，黄一宏**

  议题结论：对audio模块新增HDI接口和数据类型进行评审，新增1个耳机状态查询HDI接口，接口定义符合合规、兼容性和扩展性要求。
  
  HDI接口列表：
  
  | 接口                                        | 评审结论 |
  | ------------------------------------------- | -------- |
  | GetStatus([out] struct AudioStatus status); | 通过     |
  
  

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/GE7VA7TDQCAU4UKVEJA2NOZULKU46X5E/)

 会议议题申报：https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd

 会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
